package com.wjx.mapper;

import com.wjx.common.datasource.DS;
import com.wjx.common.datasource.DataSourceContextHolder;
import com.wjx.pojo.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Description: User类的Mapper, 采用多数据源，增删改在master，查询在slave数据源，减轻并发问题
 * @Auther: wjx
 * @Date: 2019/3/15 09:36
 */
@Mapper
public interface UserMapper {


    List<User> queryUserList();

    User queryUserByParams(User user);

    void insertUser(User user);

    void updateUser(User user);

    void deleteUser(int id);
}
