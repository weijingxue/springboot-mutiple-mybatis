package com.wjx.pojo;

import lombok.Data;

/**
 * @Description:
 * @Auther: wjx
 * @Date: 2019/3/14 18:36
 */
@Data
public class User {

    private int id;
    private String name;
    private int age;
    private String addDate;
    private String editDate;
}
