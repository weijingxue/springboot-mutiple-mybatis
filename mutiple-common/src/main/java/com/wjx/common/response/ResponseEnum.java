package com.wjx.common.response;

/**
 * @Description:
 * @Auther: wjx
 * @Date: 2019/3/15 09:44
 */

public enum ResponseEnum {

    SUCCESS("00001","success"),
    ERROR("00002","error");

    public String code;
    public String message;

    ResponseEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
