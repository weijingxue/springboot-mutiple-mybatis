package com.wjx.common.response;

import lombok.Data;
import org.springframework.util.StringUtils;

/**
 * @Description:
 * @Auther: wjx
 * @Date: 2019/3/15 09:43
 */
@Data
public class ResponseBase {

    private String resultCode;
    private String resultMessage;
    private Object object;

    private ResponseBase(String resultCode, String resultMessage, Object object) {
        this.resultCode = resultCode;
        this.resultMessage = resultMessage;
        this.object = object;
    }

    /**
     * 成功
     *
     * @return
     */
    public static ResponseBase success(Object data) {
        return new ResponseBase(ResponseEnum.SUCCESS.code, ResponseEnum.SUCCESS.message, data);
    }

    /**
     * 失败
     *
     * @param message
     * @return
     */
    public static ResponseBase error(String message) {
        return new ResponseBase(ResponseEnum.ERROR.code, StringUtils.isEmpty(message) ? ResponseEnum.ERROR.message : message, null);
    }
}
