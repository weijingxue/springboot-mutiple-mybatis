package com.wjx.common.datasource;

import java.lang.annotation.*;

/**
 * @Description: 创建一个自定义注解，用来作为切换数据源的依据
 * @Auther: wjx
 * @Date: 2019/3/15 10:26
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface DS {
    String value() default DataSourceContextHolder.MASTER_DATABASE;
}
