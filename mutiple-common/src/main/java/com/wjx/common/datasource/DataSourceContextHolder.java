package com.wjx.common.datasource;

import lombok.extern.slf4j.Slf4j;

/**
 * @Description: 创建一个ThreadLocal类来存放数据源
 * @Auther: wjx
 * @Date: 2019/3/15 10:16
 */
@Slf4j
public class DataSourceContextHolder {

    /**
     * 设置写数据源
     */
    public static final String MASTER_DATABASE = "MASTER_DATABASE";
    /**
     * 设置读数据源
     */
    public static final String SLAVE_DATABASE = "SLAVE_DATABASE";

    /**
     * 设置一个线程局部变量来存放数据源的名称
     */
    private static final ThreadLocal<String> contextLoader = new ThreadLocal<>();

    /**
     * set数据源
     *
     * @param dbName
     */
    public static void setDB(String dbName) {
        contextLoader.set(dbName);
    }

    /**
     * 获取数据源
     *
     * @return
     */
    public static String getDB() {
        return contextLoader.get();
    }

    /**
     * 清空数据源
     */
    public static void clearDB() {
        contextLoader.remove();
    }

}
