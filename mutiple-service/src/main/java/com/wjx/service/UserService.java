package com.wjx.service;

import com.wjx.common.response.ResponseBase;
import com.wjx.pojo.User;


/**
 * @Description:
 * @Auther: wjx
 * @Date: 2019/3/15 09:40
 */
public interface UserService {

    ResponseBase queryUserList();

    ResponseBase queryUserByParams(User user);

    ResponseBase insertUser(User user);

    ResponseBase updateUser(User user);

    ResponseBase deleteUser(int id);
}
