package com.wjx.service.impl;

import com.wjx.common.datasource.DS;
import com.wjx.common.datasource.DataSourceContextHolder;
import com.wjx.common.response.ResponseBase;
import com.wjx.mapper.UserMapper;
import com.wjx.pojo.User;
import com.wjx.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description:
 * @Auther: wjx
 * @Date: 2019/3/15 09:52
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    @DS(DataSourceContextHolder.SLAVE_DATABASE)
    public ResponseBase queryUserList() {
        try {
            List<User> userList = userMapper.queryUserList();
            return ResponseBase.success(userList);
        } catch (Exception e) {
            e.printStackTrace();
            ResponseBase.error(e.getMessage());
        }
        return null;
    }

    @Override
    @DS(DataSourceContextHolder.SLAVE_DATABASE)
    public ResponseBase queryUserByParams(User user) {
        try {
            User userByParams = userMapper.queryUserByParams(user);
            return ResponseBase.success(userByParams);
        } catch (Exception e) {
            e.printStackTrace();
            ResponseBase.error(e.getMessage());
        }
        return null;
    }

    @Override
    @DS
    public ResponseBase insertUser(User user) {
        try {
            userMapper.insertUser(user);
            return ResponseBase.success("新增成功");
        } catch (Exception e) {
            e.printStackTrace();
            ResponseBase.error(e.getMessage());
        }
        return null;
    }

    @Override
    @DS
    public ResponseBase updateUser(User user) {
        try {
            userMapper.updateUser(user);
            return ResponseBase.success("修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            ResponseBase.error(e.getMessage());
        }
        return null;
    }

    @Override
    @DS
    public ResponseBase deleteUser(int id) {
        try {
            userMapper.deleteUser(id);
            return ResponseBase.success("删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            ResponseBase.error(e.getMessage());
        }
        return null;
    }
}
