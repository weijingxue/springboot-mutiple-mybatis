package com.wjx.web.user;

import com.wjx.common.response.ResponseBase;
import com.wjx.pojo.User;
import com.wjx.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Description:
 * @Auther: wjx
 * @Date: 2019/3/15 10:02
 */
@RestController
public class UserController {

    @Autowired
    private UserService userService;


    @GetMapping("/queryUserList")
    public ResponseBase queryUserList(){
        return userService.queryUserList();
    }

    @PostMapping("/queryUserByParams")
    public ResponseBase queryUserList(@RequestBody User user){
        return userService.queryUserByParams(user);
    }

    @PostMapping("/insertUser")
    public ResponseBase insertUser(@RequestBody User user){
        return userService.insertUser(user);
    }

    @PostMapping("/updateUser")
    public ResponseBase updateUser(@RequestBody User user){
        return userService.updateUser(user);
    }

    @DeleteMapping("/deleteUser")
    public ResponseBase deleteUser(int id){
        return userService.deleteUser(id);
    }
}
