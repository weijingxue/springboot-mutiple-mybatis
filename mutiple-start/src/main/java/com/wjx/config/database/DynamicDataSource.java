package com.wjx.config.database;

import com.wjx.common.datasource.DataSourceContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * @Description: 继承AbstractRoutingDataSource类，来重写数据源获取方式
 * @Auther: wjx
 * @Date: 2019/3/15 10:22
 */
@Slf4j
public class DynamicDataSource extends AbstractRoutingDataSource {
    @Override
    protected Object determineCurrentLookupKey() {
        //获取当前的数据源
        String db = DataSourceContextHolder.getDB();
        log.info("==>  AbstractRoutingDataSource当前的数据源是：{}",db);
        return db;
    }
}
